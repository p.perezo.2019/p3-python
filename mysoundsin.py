import math
from mysound import Sound


class SoundSin(Sound):
    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)

        # Añade el atributo "samples_second" a SoundSin
        self.samples_second = Sound.samples_second

        # Genera una señal sinusoidal en el buffer de SoundSin
        # Inicializa un buffer vacío
        self.buffer = [0] * self.nsamples
        # Genera la señal sinusoidal en el buffer
        self.sin(frequency, amplitude)


if __name__ == '__main__':
    # Crea un objeto SoundSin con una señal sinusoidal de 1 segundo,
    sound_sin = SoundSin(1, 440, 10000)

    # Para acceder al atributo "samples_second" de SoundSin
    print(sound_sin.samples_second)

    print(sound_sin.bars())
