from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:
    # Determina la longitud del sonido resultante
    min_length = min(len(s1.buffer), len(s2.buffer))
    max_length = max(len(s1.buffer), len(s2.buffer))

    # Inicializa un nuevo sonido resultante con la longitud máxima
    result = Sound(max_length / s1.samples_second)

    # Suma las muestras de los sonidos
    for i in range(min_length):
        result.buffer[i] = s1.buffer[i] + s2.buffer[i]

    # Copia las muestras restantes del sonido más largo
    if len(s1.buffer) > len(s2.buffer):
        result.buffer[min_length:] = s1.buffer[min_length:]
    else:
        result.buffer[min_length:] = s2.buffer[min_length:]

    return result
