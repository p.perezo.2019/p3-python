import unittest
from mysound import Sound
from soundops import soundadd


class TestSoundAdd(unittest.TestCase):

    def test_same_length(self):
        # Prueba que la suma de dos sonidos del mismo tamaño sea correcta
        s1 = Sound(1)
        s1.sin(440, 10000)

        s2 = Sound(1)
        s2.sin(220, 5000)

        result = soundadd(s1, s2)

        self.assertEqual(len(s1.buffer), len(result.buffer))

        # Verifica que la suma de las muestras sea correcta
        for i in range(len(s1.buffer)):
            self.assertEqual(result.buffer[i], s1.buffer[i] + s2.buffer[i])

    def test_different_length(self):
        # Prueba que la suma de dos sonidos de diferentes tamaños sea correcta
        s1 = Sound(1)
        s1.sin(440, 10000)

        s2 = Sound(2)
        s2.sin(220, 5000)

        result = soundadd(s1, s2)

        # El sonido resultante debe tener la longitud del sonido más largo
        self.assertEqual(len(result.buffer), max(len(s1.buffer), len(s2.buffer)))

        # Verifica que las muestras sean correctas para la longitud común
        for i in range(min(len(s1.buffer), len(s2.buffer))):
            self.assertEqual(result.buffer[i], s1.buffer[i] + s2.buffer[i])

    def test_empty_sound(self):
        # Prueba que la suma con un sonido vacío devuelva el otro sonido
        s1 = Sound(1)
        s1.sin(440, 10000)

        s2 = Sound(0)  # Sonido vacío

        result = soundadd(s1, s2)

        # El sonido resultante debe ser igual a s1
        self.assertEqual(result.buffer, s1.buffer)


if __name__ == '__main__':
    unittest.main()
