import unittest
from mysoundsin import SoundSin


class TestSoundSin(unittest.TestCase):

    def test_creation(self):
        # Prueba que se puede crear un objeto SoundSin correctamente
        sound_sin = SoundSin(1, 440, 10000)
        self.assertIsInstance(sound_sin, SoundSin)

    def test_duration(self):
        # Prueba que la duración de SoundSin coincide con la especificada
        sound_sin = SoundSin(2, 220, 8000)
        self.assertEqual(2, sound_sin.duration)

    def test_amplitude(self):
        # Prueba que la amplitud de la señal sinusoidal es correcta
        sound_sin = SoundSin(1, 440, 5000)
        max_amplitude = SoundSin.max_amplitude
        self.assertTrue(all(
            -SoundSin.max_amplitude <= x <= SoundSin.max_amplitude
            for x in sound_sin.buffer
        ))


if __name__ == '__main__':
    unittest.main()
