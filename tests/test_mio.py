import unittest
from mysound import Sound

class TestSound(unittest.TestCase):

     def test_duration(self):
        # Crea una instancia de Sound con duración de 2.5 segundos
        sound = Sound(2.5)

        # Verifica que la duración del sonido sea igual a 2.5 segundos
        self.assertEqual(2.5, sound.duration)


if __name__ == '__main__':
    unittest.main()

